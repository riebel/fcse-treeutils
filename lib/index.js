'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

require('babel-polyfill');

var _immutableTreeutils = require('immutable-treeutils');

var _immutableTreeutils2 = _interopRequireDefault(_immutableTreeutils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FCSETreeUtils = function (_TreeUtils) {
  _inherits(FCSETreeUtils, _TreeUtils);

  function FCSETreeUtils() {
    _classCallCheck(this, FCSETreeUtils);

    return _possibleConstructorReturn(this, (FCSETreeUtils.__proto__ || Object.getPrototypeOf(FCSETreeUtils)).apply(this, arguments));
  }

  _createClass(FCSETreeUtils, null, [{
    key: 'insert',
    value: function insert(state, newNode, parentId, index) {
      return state.updateIn(state.getById(state, parentId).concat('childNodes'), function (childNodes) {
        return childNodes.splice(index, 0, newNode);
      });
    }
  }]);

  return FCSETreeUtils;
}(_immutableTreeutils2.default);

exports.default = FCSETreeUtils;
module.exports = exports['default'];