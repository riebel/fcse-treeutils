const path = require('path');
const FlowStatusWebpackPlugin = require('flow-status-webpack-plugin');

module.exports = {
  entry: ['babel-polyfill', './src/index.js'],
  output: {
    path: path.resolve(__dirname, 'lib'),
    filename: 'index.js',
    library: "fcse-treeutils",
    libraryTarget: "umd"
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      }
    ]
  },
  plugins: [
    new FlowStatusWebpackPlugin({
      binaryPath: 'node_modules/.bin/flow',
      onSuccess: console.log,
      onError: console.error
    })
  ]
};