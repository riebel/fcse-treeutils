FCSE Immutable TreeUtils
===================

0.1.0

This CommonJS module is a collection of helpers to write or change [ImmutableJS](http://facebook.github.io/immutable-js/) tree data structure with a DOM-inspired interface.

It is written in ES2015, the ES5/ES3 distribution is built with [Babel](babeljs.io).

### Install and setup

Install the package from [Bitbucket](https://bitbucket.org/riebel/fcse-treeutils):

```
npm install git+ssh://git@bitbucket.org/riebel/fcse-treeutils.git
```