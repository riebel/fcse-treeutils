// @flow
import 'babel-polyfill';
import TreeUtils from 'immutable-treeutils';

export default class FCSETreeUtils extends TreeUtils {
  static insert(state: any, newNode: any, parentId: string | number, index: number): any {
    return state.updateIn(
      state.getById(state, parentId).concat('childNodes'),
      childNodes => childNodes.splice(index, 0, newNode)
    );
  }
}
