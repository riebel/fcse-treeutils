const Immutable = require('immutable');
const FCSETreeUtils = require('../src/index');

const { Seq } = Immutable;

describe('class `FCSETreeUtils`', () => {
  const fixtures = {
    data: {
      name: 'Article',
      type: 'article',
      id: '1',
      childNodes: [
        {
          type: 'paragraph',
          name: 'Paragraph',
          id: '2'
        },
        {
          type: 'list',
          name: 'List',
          id: '3',
          childNodes: [
            {
              type: 'listItem',
              name: 'List item 1',
              id: '4',
              childNodes: [
                {
                  type: 'paragraph',
                  name: 'Nested paragraph',
                  id: '5'
                }
              ]
            },
            {
              type: 'listItem',
              name: 'List item 2',
              id: '6',
              childNodes: [
                {
                  type: 'paragraph',
                  name: 'Nested paragraph 2',
                  id: '7'
                }
              ]
            }
          ]
        }
      ]
    }
  };
  const state = Immutable.fromJS(fixtures);
  const utils = new FCSETreeUtils(Seq.of('data'));

  describe('method `insert`', () => {
    it('exists', () => {
      expect(FCSETreeUtils.insert).toBeDefined();
    });
  });

});